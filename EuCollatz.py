import pdb

def y(i):
    if i ==1:
        return 1
    elif i%2 == 0: 
        return y(i/2)+1
    else: 
        return y(3*i+1)+1

max_i=1
n = 0 
for i in range(5,1000000):
    yi = y(i)
    if yi>n:
      n = yi
      max_i = i 
pdb.set_trace()

print (n ," by number", max_i)